# AWS Account Config

Personal AWS account config

### Step 1
```
$ aws cloudformation create-stack --stack-name BaseResources --template-body file://$PWD/base-cfn/resources.yml --parameters ParameterKey=BucketName,ParameterValue=*new_tfstate_bucketname*
```

### Step 2
Change bucket name given above, to the bucket name in `base-tfconfig/providers.tf` under the terraform backend config

### Step 3
```
$ terraform plan -var nat=true
# variable nat=true is if you want NAT gateway resources which cost $$$$
```

