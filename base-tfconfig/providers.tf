provider "aws" {
  region = "us-east-2"
}

terraform {
  backend "s3" {
    bucket = "${var.backend_bucket_name}"
    key    = "account.tfstate"
    region = "us-east-2"
  }
}

variables "backend_bucket_name" {}